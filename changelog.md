# Changelog

#### 20181201.1
* Change: repo location
* Updated: description

#### 20151113.1
* Changed: Code cleaning
* Bumped version

#### #### 20150724.1
* Updated: gitignore file

#### 20150705.1
* Fix: localization
* Added: more entries to plugin description.

#### 20150617.1
* Change: code cleanup

#### 20150322
* Added: security checks.

#### 20150201
* Changed: the menu name on single site name.

#### 20141101.1
* updated the plugin description
* tighten the code

#### 20140825.1
* converted changelog to markdown
* added git url tags to plugin description block
* added license text file
* updated readme

#### 20140823.1
* updated readme.md

#### 20140714.1
* code check
* fixed comment spelling

#### 20140712.1
* created the plugin.