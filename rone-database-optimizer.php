<?php
/*
 * @category            WordPress_Plugin
 * @package             RONE Database Optimizer
 * @author              Tim Russell <tim@timrussell.com>
 * @copyright           Copyright (c) 2014-2015.
 * @license             GPL-2.0+
 *
 *
 * @wordpress-plugin
 *
 * Plugin Name:         RONE Database Optimizer
 * Plugin URI:          https://gitlab.com/tdavidrussell/rone-database-optimizer
 * Description:         Database Optimizer. Single and Multisite capable
 * Version:             20181201.1
 * Author:              Tim Russell
 * Author URI:          http://www.timrussell.com/
 * License:             GPLv2 or later
 * License URI:         http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:         rone-database-optimizer
 * Domain Path:         /languages
 *
 * Tags:
 *
 * GitLab Plugin URI:   https://gitlab.com/tdavidrussell/rone-database-optimizer
 * GitLab Branch:       master
 *
 * Support URI:         https://gitlab.com/tdavidrussell/rone-database-optimizer
 * Documentation URI:   https://gitlab.com/tdavidrussell/rone-database-optimizer
 *
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * **********************************************************************
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/** initial constants **/
define( 'RODBO_PLUGIN_VERSION', '20181201.1' );
define( 'RODBO_PLUGIN_DEBUG', false );
//
define( 'RODBO_PLUGIN_URI', plugin_dir_url( __FILE__ ) ); //Does contain trailing slash
define( 'RODBO_PLUGIN_PATH', plugin_dir_path( __FILE__ ) ); //Does contain trailing slash


/**
 * Format table/file size to be user readable
 *
 * @since 2014-04-18
 *
 * @access private
 *
 **/
function rone_dbopt_table_format_size( $size ) {
	if ( $size >= 1073741824 ) {
		$size = number_format( round( $size / 1073741824 * 100 ) / 100, 2 ) . " Gig";
	} elseif ( $size >= 1048576 ) {
		$size = number_format( round( $size / 1048576 * 100 ) / 100, 2 ) . " Meg";
	} elseif ( $size >= 1024 ) {
		$size = number_format( round( $size / 1024 * 100 ) / 100, 2 ) . " Kb";
	} else {
		$size = $size . " Bytes";
	}
	//
	if ( $size == 0 ) {
		$size = "-0-";
	}

	return $size;
}

/**
 * Database Optimizer (MySQL)
 *
 * @since 2010-01-12
 *
 * @access public
 *
 **/
function rone_dbopt_db_optimize() {
	global $wpdb;

	?>
	<div class="wrap">
		<div id="icon-tools" class="icon32">&nbsp;</div>
		<h2><? _e( "Database Optimizer" ) ?></h2>

		<h3><?php _e( "Database: " );
			echo DB_NAME; ?></h3>

		<table class="wp-list-table widefat fixed" align="center" border="0" cellpadding="2" cellpacing="0" width="80%">
			<thead>
			<!--
                    <tr><td align="center" colspan="6"><h2><?php _e( "Database Optimized" ); ?></h2></td></tr>
                <tr><td align="center" colspan="6"><?php _e( "Database: " );
			echo DB_NAME; ?></td></tr>
                <tr><td align="center" colspan="6">&nbsp;</td></tr>
                -->
			<tr>
				<th style="text-align: center;" score="col"><?php _e( "Table" ); ?></th>
				<th score="col" style="text-align: right;"><?php _e( "Size" ); ?></th>
				<th score="col" style="text-align: center;"><?php _e( "Status" ); ?></th>
				<th score="col" style="text-align: center;"><?php _e( "Space Saved" ); ?></th>
				<th score="col" style="text-align: center;"><?php _e( "Rows" ); ?></th>
				<th score="col" style="text-align: center;"><?php _e( "Last Updated" ); ?></th>
			</tr>
			</thead>
			<tbody>
			<?php
			$db_clean     = $dbname;
			$tot_data     = 0;
			$tot_idx      = 0;
			$tot_all      = 0;
			$local_query  = "SHOW TABLE STATUS FROM " . DB_NAME . " ";
			$result       = $wpdb->get_results( $local_query );
			$result_count = count( $result );
			//
			if ( $result_count > 0 ) {
				$altrow = 1;
				foreach ( $result as $row ) {
					$tblname  = $row->Name;
					$tot_data = $row->Data_length;
					$tot_idx  = $row->Index_length;
					$total    = rone_dbopt_table_format_size( $tot_data + $tot_idx );
					$gain     = $row->Data_free;
					$gain     = $gain / 1024;
					$total_gain += $gain;
					$gain        = number_format( round( $gain, 3 ), 4 );
					$rowcount    = number_format( $row->Rows );
					$lastupdate  = $row->Update_time;
					$total_gain  = number_format( $total_gain, 4 );
					$local_query = "OPTIMIZE TABLE $tblname ";
					$resultat    = $wpdb->get_results( $local_query );
					if ( $altrow % 2 ) {
						$altclass = "alternate";
					} else {
						$altclass = "";

					}
					if ( $gain == 0 ) {
						?>
						<tr class="<?php echo $altclass; ?>">
							<td scope="row"><?php echo $tblname; ?></td>
							<td align="right"><?php echo $total; ?></td>
							<td align="center"><?php _e( "Already Optimized" ); ?></td>
							<td align="center">0 Kb</td>
							<td align="center"><?php echo $rowcount; ?></td>
							<td align="center"><?php echo $lastupdate; ?> </td>
						</tr>
					<?php } else { ?>
						<tr class="<?php echo $altclass; ?>">
							<td><?php echo $tblname; ?></td>
							<td align="right"><strong><?php echo $total; ?></strong></td>
							<td align="center"><strong><?php _e( "Optimized" ); ?></strong></td>
							<td align="center"><strong><?php echo $gain; ?> Kb</strong></td>
							<td align="center"><?php echo $rowcount; ?></td>
							<td align="center"><?php echo $lastupdate; ?> </td>
						</tr>
						<?php
					}
					$altrow ++;
				}
			}
			?>
			</tbody>
		</table>
	</div>
	<?php
}

/**
 * Adds the link to the Administration / Tools Menu for a single site install.
 *
 * @since 2014-04-18
 *
 * @access public
 *
 **/
function rone_dbopt_add_menus() {

	add_management_page( __( 'Database Optimizer', 'rone_db_optimize' ), __( 'Database Optimizer', 'rone_dbopt_db_optimize' ), 'manage_options', 'rone_dbopt_db_optimize', 'rone_dbopt_db_optimize' );

}

/**
 * Adds the link to the Network Admin / Settings menu on Multisite Install
 *
 * @since 2014-04-18
 *
 * @access public
 *
 **/
function rone_dbopt_add_menus_ms() {

	add_submenu_page( 'settings.php', 'Database Optimizer', 'Database Optimizer', 'manage_options', 'rone_dbopt_db_optimize', 'rone_dbopt_db_optimize' );

}

// If multisite install
if ( is_multisite() ) {

	add_action( 'network_admin_menu', 'rone_dbopt_add_menus_ms' );

} else {
	// not multisite install and the user is admin
	if ( is_admin() ) {
		add_action( 'admin_menu', 'rone_dbopt_add_menus' );
	}
}
