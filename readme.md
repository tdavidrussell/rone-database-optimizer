## RONE Database Optimizer  [Retired Plugin]

Database Optimizer For WordPress.

__Contributors:__ [Tim Russell](https://gitlab.com/tdavidrussell)   
__Requires:__ 5.4   
__Tested up to:__ 5.6   
__License:__ [GPL-2.0+](http://www.gnu.org/licenses/gpl-2.0.html)   


### Raging One Database Optimizer For WordPress [Retired Plugin]


![Embedded Optimizer Screenshot](screenshot-1.png)

_Example optimized database._

## License   
This program is distributed in the hope that it will be useful, but WITHOUT ANY 
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR 
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

## Installation ##

### Upload ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/rone-database-optimizer/) from GitLab.
2. Go to the __Plugins &rarr; Add New__ screen in your WordPress admin panel and click the __Upload__ tab at the top.
3. Upload the zipped archive.
4. Click the __Activate Plugin__ link after installation completes.

### Manual ###

1. Download the [latest release](https://gitlab.com/tdavidrussell/rone-database-optimizer/) from GitLab.
2. Unzip the archive.
3. Copy the folder to `/wp-content/plugins/`.
4. Go to the __Plugins__ screen in your WordPress admin panel and click the __Activate__ link under RONE Database Optimizer.

Read the Codex for more information about [installing plugins manually](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).


## Usage ##

### Single Site Install ###
* Under single site only the administrator has access.
* On a single site installation, the link is under Administration / Setting.

### Multi Site Install ###
* Under Multi Site only the Network Administrator has access.
* On Multi Site installation, the link is under Network Admin / Tools.

## Known Issues ##
The database name and tables can only have alpha [a-zA-Z], numeric [0-9] characters and underscores '_', no spaces or special characters.


## Changelog

See [CHANGELOG](changelog.md).

